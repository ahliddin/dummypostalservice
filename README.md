*Dummy Postal Service* application to process postal orders entered on command line.

*MainApp* - is the entry point to the application.  
The application can be started with two optional arguments:   
- first argument is the file name containing orders to be processed after startup  
- second argument is the file name containing fees per weight  

The file name arguments should use either absolute path or relative to project root folder.  
The *data/* folder contains two example files *orders.txt* and *fees.txt*

When file with orders is passed the application will do the best effort to import all valid orders.
Any invalid orders are logged as error messages in log file and user is notified via console interface.

The file with fees is used only if fully validated.  
Requirements for fee file:  
- the weights should be in descending order (from biggest weight to smallest).  
- the weights should be unique

The minimum order fee is the fee of the smallest weight


##### How to run commands 
Once the application is started enter the order commands in below format:  
`<weight: positive number, >0, maximal 3 decimal places, . (dot) as decimal separator><space><postal code: fixed 5 digits>`\
*Example: 3.4 08801*

Processed orders are printed every minute.  
To print processed orders any time enter *print*.  
To stop the application enter *quit*.  

##### How to start the app from command line

The below commands need to be executed from the root folder (where *pom.xml* file located).  

Run the app without parameters:  
`mvn compile exec:java -Dexec.mainClass=cz.bsc.external.MainApp`

Run the app with parameters:  
`mvn compile exec:java -Dexec.mainClass=cz.bsc.external.MainApp -Dexec.args="data/orders.txt data/fees.txt"`

