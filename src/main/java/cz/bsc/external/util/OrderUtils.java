package cz.bsc.external.util;

import com.google.common.collect.Range;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderUtils {
    private static final Logger LOG = LoggerFactory.getLogger(OrderUtils.class);


    public static boolean isValidOrderInput(String inputLine) {
        if (StringUtils.isEmpty(inputLine)) {
            LOG.error("Empty command line");
            return false;
        }

        if (!inputLine.matches("[0-9]+(\\.[0-9]{0,3})? [0-9]{5}")) {
            LOG.error("Order line \"{}\" doesn't match the defined format", inputLine);
            return false;
        }

        double weight = Double.parseDouble(StringUtils.split(inputLine)[0]);
        if (weight == 0) {
            LOG.error("Weight cannot be zero");
            return false;
        }

        return true;
    }

    public static boolean isValidFeeFile(Path feeFile) {
        try {
            List<String> feeInputs = Files.readAllLines(feeFile);
            Collections.reverse(feeInputs); // so we can compare weight in each new line is strictly bigger
            double prevWeight = 0;
            for (String feeInput : feeInputs) {
                if (StringUtils.isBlank(feeInput)) {
                    continue;
                }
                if (!isValidFeeInput(feeInput)) {
                    return false;
                }
                String[] feeTokens = StringUtils.split(feeInput);
                double weight = Double.parseDouble(feeTokens[0]);
                if (prevWeight >= weight) {
                    LOG.error("Weight in each new line should be strictly smaller than weight in previous line. Previous weight: {}, new weight: {}", prevWeight, weight);
                    return false;
                }
                prevWeight = weight;
            }

        } catch (IOException e) {
            LOG.error("Error while opening fee file", e);
            return false;
        }

        return true;
    }

    static boolean isValidFeeInput(String feeInput) {
        if (!feeInput.matches("[0-9]+(\\.[0-9]{0,3})? [0-9]+(\\.[0-9]{0,2})")) {
            LOG.error("Fee input has incorrect format: \"{}\"", feeInput);
            return false;
        }

        return true;
    }

    public static Map<Range<Double>, BigDecimal> getRangeFees(Path feeFile) {
        try {
            Map<Range<Double>, BigDecimal> rangeFees = new HashMap<>();
            List<String> feeInputs = Files.readAllLines(feeFile);
            Double lastWeight = null;
            BigDecimal lastFee = null;
            boolean firstRound = true;
            for (String feeInput : feeInputs) {
                if (StringUtils.isBlank(feeInput)) {
                    continue;
                }
                String[] feeTokens = StringUtils.split(feeInput);
                Double weight = Double.valueOf(feeTokens[0]);
                BigDecimal fee = new BigDecimal(feeTokens[1]);
                if (firstRound) {
                    rangeFees.put(Range.atLeast(weight), fee); //the highest fee for biggest weight
                    firstRound = false;
                } else {
                    rangeFees.put(Range.closedOpen(weight, lastWeight), fee);
                }
                lastWeight = weight;
                lastFee = fee;
            }
            if (lastWeight != null && lastFee != null) {
                rangeFees.put(Range.openClosed(0.0, lastWeight), lastFee); // minimum fee is fee of the smallest weight
            }
            return rangeFees;

        } catch (IOException e) {
            LOG.error("Exception while processing fee file", e);
            return Collections.emptyMap();
        }
    }

}
