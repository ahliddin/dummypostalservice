package cz.bsc.external.util;

import cz.bsc.external.order.ExtendedOrder;
import cz.bsc.external.order.PostalOrder;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;
import java.util.Queue;

import static java.util.stream.Collectors.toMap;

/**
 * Utility class to print postal orders to console.
 */
public class PrintingUtils {

    /**
     * Depending on type of {@link PostalOrder} implementation prints orders either with fee information or without.
     */
    public static void printOrders(Queue<PostalOrder> ordersContainer) {
        if (ordersContainer.isEmpty()) {
            System.out.println("No orders found");
        }

        if (containsOnlyExtendedOrders(ordersContainer)) {
            printWithFeeInfo(ordersContainer);
        } else {
            printWithoutFeeInfo(ordersContainer);
        }

    }

    /**
     * Groups elements by postalCode.
     * For orders with the same postalCode the weights summed.
     * Additional sorting is done to print the orders with biggest weight first.
     */
    private static void printWithoutFeeInfo(Queue<PostalOrder> ordersContainer) {
        Map<String, Double> postalCodeWithTotalWeights = ordersContainer.stream()
                .collect(toMap(PostalOrder::getPostalCode, PostalOrder::getWeight, Double::sum));

        postalCodeWithTotalWeights.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .forEach(e -> System.out.printf("%s %.3f\n", e.getKey(), e.getValue()));
    }

    /**
     * Casts every {@link PostalOrder} in container to {@link ExtendedOrder} then groups elements by postalCode.
     * For orders with the same postalCode the weights and fees are summed.
     * Additional sorting is done to print the orders with highest fee first.
     */
    private static void printWithFeeInfo(Queue<PostalOrder> ordersContainer) {
        Map<String, WeightFee> postalCodeWithTotalWeightAndFee = ordersContainer.stream()
                .filter(o -> o instanceof ExtendedOrder) // additional check before casting
                .map(o -> (ExtendedOrder) o)
                .collect(toMap(ExtendedOrder::getPostalCode,
                        eo -> new WeightFee(eo.getWeight(), eo.getDeliveryFee()),
                        (wf1, wf2) -> new WeightFee(wf1.weight + wf2.weight, wf1.fee.add(wf2.fee))));


        postalCodeWithTotalWeightAndFee.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparing((WeightFee wf) -> wf.fee).reversed()))
                .forEach(e -> {
                    System.out.printf("%s %.3f %.2f\n", e.getKey(), e.getValue().weight, e.getValue().fee);
                });
    }

    private static boolean containsOnlyExtendedOrders(Queue<PostalOrder> ordersContainer) {
        return ordersContainer.stream()
                .allMatch(e -> e instanceof ExtendedOrder);
    }

    /**
     * Tuple alternative to hold weight and fee pair
     */
    private static class WeightFee {
        final double weight;
        final BigDecimal fee;

        public WeightFee(double weight, BigDecimal fee) {
            this.weight = weight;
            this.fee = fee;
        }
    }

}
