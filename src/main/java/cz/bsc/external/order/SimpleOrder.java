package cz.bsc.external.order;

/**
 * Simple implementation of PostalOrder
 */
public class SimpleOrder implements PostalOrder {
    private final double weight;
    private final String postalCode;

    public SimpleOrder(double weight, String postalCode) {
        this.weight = weight;
        this.postalCode = postalCode;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public String getPostalCode() {
        return postalCode;
    }
}
