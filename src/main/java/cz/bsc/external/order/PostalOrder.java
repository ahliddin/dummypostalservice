package cz.bsc.external.order;

/**
 * Represents a dummy postal order information with minimal data.
 */
public interface PostalOrder {
    double getWeight();

    String getPostalCode();
}
