package cz.bsc.external.order;


import java.math.BigDecimal;

/**
 * Order that extends {@link SimpleOrder} to add fee information.
 */
public class ExtendedOrder extends SimpleOrder {
    private final BigDecimal deliveryFee;

    public ExtendedOrder(double weight, String postalCode, BigDecimal deliveryFee) {
        super(weight, postalCode);
        this.deliveryFee = deliveryFee.setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    public BigDecimal getDeliveryFee() {
        return deliveryFee;
    }
}
