package cz.bsc.external;

import com.google.common.collect.Range;
import cz.bsc.external.order.PostalOrder;
import cz.bsc.external.util.OrderUtils;
import cz.bsc.external.util.PrintingUtils;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * Main entry point to the application.
 */
public class MainApp {

    private static final int PRINT_DELAY_MINS = 1;
    private static final int PRINT_PERIOD_MINS = 1;

    public static void main(String[] args) {
        Queue<PostalOrder> orderContainer = new ConcurrentLinkedQueue<>();
        Timer timer = new Timer("Scheduled printer");
        TimerTask printTask = new PrintTask(orderContainer);
        timer.schedule(printTask, MINUTES.toMillis(PRINT_DELAY_MINS), MINUTES.toMillis(PRINT_PERIOD_MINS));

        Map<Range<Double>, BigDecimal> rangeFees = Collections.emptyMap();
        if (isFeeFileProvided(args)) {
            Path feeFile = Paths.get(args[1]);
            if (!OrderUtils.isValidFeeFile(feeFile)) {
                System.err.println("Given fee file is invalid. Check log file for more details.");
            } else {
                rangeFees = OrderUtils.getRangeFees(feeFile);
            }
        }

        DummyPostalService service = new DummyPostalService(orderContainer, rangeFees);
        if (isOrderFileProvided(args)) {
            Path orderFile = Paths.get(args[0]);
            if (!service.processInBatch(orderFile)) {
                System.err.println("Error(s) occurred while importing orders from file. Check log file for details.");
            }
        }

        service.startCmdProcessing();
        timer.cancel();
    }

    /**
     * Fee file should be passed as second argument to the application.
     * Any other parameters passed to the application after fee file are ignored.
     */
    private static boolean isFeeFileProvided(String[] args) {
        return args.length >= 2;
    }

    /**
     * Order file should be provided as first argument to the application.
     */
    private static boolean isOrderFileProvided(String[] args) {
        return args.length >= 1;
    }

    /**
     * Tiny implementation of {@link TimerTask} which is passed to scheduler to print all processed orders.
     */
    private static class PrintTask extends TimerTask {
        private final Queue<PostalOrder> orderContainer;

        private PrintTask(Queue<PostalOrder> orderContainer) {
            this.orderContainer = orderContainer;
        }

        @Override
        public void run() {
            PrintingUtils.printOrders(orderContainer);
        }
    }
}
