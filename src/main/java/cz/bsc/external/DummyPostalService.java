package cz.bsc.external;


import com.google.common.collect.Range;
import cz.bsc.external.order.ExtendedOrder;
import cz.bsc.external.order.PostalOrder;
import cz.bsc.external.order.SimpleOrder;
import cz.bsc.external.util.OrderUtils;
import cz.bsc.external.util.PrintingUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

/**
 * Service that processes {@link PostalOrder}s.
 * Orders can be processed either from command line one-by-one or from input file in batches.
 */
public class DummyPostalService {
    private static final Logger LOG = LoggerFactory.getLogger(DummyPostalService.class);
    private static final String CMD_QUIT = "quit";
    private static final String CMD_PRINT = "print";

    private final Queue<PostalOrder> orderContainer;
    private final Map<Range<Double>, BigDecimal> rangeFees;

    public DummyPostalService(Queue<PostalOrder> orderContainer, Map<Range<Double>, BigDecimal> rangeFees) {
        this.orderContainer = orderContainer;
        this.rangeFees = rangeFees;
    }

    /**
     * Attempts to process all valid orders in given file.
     * Any invalid order in file is logged as error message in log file.
     * Empty lines are ignored.
     *
     * @return false if at least one order was incorrectly processed, otherwise returns true
     */
    public boolean processInBatch(Path filePath) {
        boolean allProcessedSuccessfully = true;
        try {
            for (String orderInput : Files.readAllLines(filePath)) {
                if (StringUtils.isBlank(orderInput)) {
                    continue; // empty lines are allowed and ignored
                }
                if (!processOrder(orderInput)) {
                    LOG.error("Importing from file - incorrect order input: \"{}\", ignoring it.", orderInput);
                    allProcessedSuccessfully = false;
                }
            }
        } catch (IOException e) {
            LOG.error("Error occurred while accessing file", e);
            return false;
        }

        return allProcessedSuccessfully;
    }

    /**
     * The service runs indefinitely and reads order commands from console until 'quit' command is received.
     */
    public void startCmdProcessing() {
        System.out.println("Enter order in format: <weight:decimal> <zip code:5 digits>");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String orderInput = scanner.nextLine();
            if (orderInput.equalsIgnoreCase(CMD_QUIT)) {
                break;
            }
            if (StringUtils.isBlank(orderInput)) {
                continue;
            }
            if (orderInput.equalsIgnoreCase(CMD_PRINT)) {
                PrintingUtils.printOrders(orderContainer);
                continue;
            }
            if (processOrder(orderInput)) {
                System.out.println("Package processed successfully");
            } else {
                System.out.println("Error. Input format: <weight: more than 0; . (dot) as decimal separator; maximal 3 decimal places><space><postal code: fixed 5 digits>");
            }
        }
        LOG.debug("The service quit processing input commands.");
        System.out.println("Bye-bye..");
    }

    boolean processOrder(String orderInput) {
        if (!OrderUtils.isValidOrderInput(orderInput)) {
            return false;
        }

        String[] orderTokens = StringUtils.split(orderInput);
        double weight = Double.parseDouble(orderTokens[0]);
        String postalCode = orderTokens[1];


        PostalOrder order;
        if (!rangeFees.isEmpty()) {
            BigDecimal fee = getFeeForWeight(weight);
            order = new ExtendedOrder(weight, postalCode, fee);
        } else {
            order = new SimpleOrder(weight, postalCode);
        }

        orderContainer.add(order);

        LOG.debug("Successfully processed order command \"{}\"", orderInput);
        return true;
    }

    /**
     * Looks up a fee for the the weight of the postal order.
     * If no fee is found for the given weight {@link IllegalStateException} is thrown.
     */
    private BigDecimal getFeeForWeight(double weight) {
        Range<Double> range = rangeFees.keySet().stream()
                .filter(r -> r.contains(weight))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Fee range is not found for weight: " + weight));

        return rangeFees.get(range);
    }

}

