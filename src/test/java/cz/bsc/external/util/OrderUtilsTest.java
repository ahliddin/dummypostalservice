package cz.bsc.external.util;

import com.google.common.collect.Range;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class OrderUtilsTest {

    @Test
    void testCommandInputs() {

        assertTrue(OrderUtils.isValidOrderInput("3.4 08801"), "valid input");
        assertTrue(OrderUtils.isValidOrderInput("2 90005"), "valid input");
        assertTrue(OrderUtils.isValidOrderInput("12.56 08801"), "valid input");
        assertTrue(OrderUtils.isValidOrderInput("5.5 08079"), "valid input");
        assertTrue(OrderUtils.isValidOrderInput("3.2 09300"), "valid input");

        assertFalse(OrderUtils.isValidOrderInput("3.2  09300"), "invalid input - extra space");
        assertFalse(OrderUtils.isValidOrderInput("3.2. 09300"), "invalid input - extra dot");
        assertFalse(OrderUtils.isValidOrderInput("3.2222 09300"), "invalid input - 4 decimal places");
        assertFalse(OrderUtils.isValidOrderInput("3,333 09300"), "invalid input - comma instead of dot");
        assertFalse(OrderUtils.isValidOrderInput("3 109300"), "invalid input - 6 digit postal code");
        assertFalse(OrderUtils.isValidOrderInput("0 09300"), "invalid input - zero weight");
        assertFalse(OrderUtils.isValidOrderInput(".123 09300"), "invalid input - missing digit before decimal point");
    }

    @Test
    void testFeeInputs() {
        assertTrue(OrderUtils.isValidFeeInput("0.001 0.12"), "valid input");
        assertTrue(OrderUtils.isValidFeeInput("1.001 1111.12"), "valid input");
        assertTrue(OrderUtils.isValidFeeInput("01 0.12"), "valid input");

        assertFalse(OrderUtils.isValidFeeInput(".001 0.12"), "invalid fee input - missing digit before decimal point");
        assertFalse(OrderUtils.isValidFeeInput("00.001 .12"), "invalid fee input - missing digit before decimal point");
        assertFalse(OrderUtils.isValidFeeInput("0.0012 0.12"), "invalid fee input - 4 decimal places");
        assertFalse(OrderUtils.isValidFeeInput("0.001 0.123"), "invalid fee input - 3 decimal places in price");
    }


    @Test
    void testValidFeeFile() throws IOException {
        Path feeFile = Files.createTempFile("postalservice", "test");
        FileWriter writer = new FileWriter(feeFile.toFile());
        writer.append("10 5.00\n");
        writer.append("5 2.50\n");
        writer.append("2 1.50\n");
        writer.append("1 1.00\n");
        writer.append("\n");
        writer.flush();

        assertTrue(OrderUtils.isValidFeeFile(feeFile));
    }

    @Test
    void testFeeFileWithEqualWeights() throws IOException {
        Path feeFile = Files.createTempFile("postalservice", "test");
        FileWriter writer = new FileWriter(feeFile.toFile());
        writer.append("10 5.00\n");
        writer.append("5 2.50\n");
        writer.append("2 1.50\n");
        writer.append("2 1.00\n");
        writer.flush();

        assertFalse(OrderUtils.isValidFeeFile(feeFile));
    }

    @Test
    void testFeeFileWithIncorrectWeight() throws IOException {
        Path feeFile = Files.createTempFile("postalservice", "test");
        FileWriter writer = new FileWriter(feeFile.toFile());
        writer.append("10.0000 5.00\n");
        writer.flush();

        assertFalse(OrderUtils.isValidFeeFile(feeFile));
    }

    @Test
    void testFeeFileWithIncorrectFee() throws IOException {
        Path feeFile = Files.createTempFile("DummyPostalService", "test.txt");
        FileWriter writer = new FileWriter(feeFile.toFile());
        writer.append("10.000 5.001\n");
        writer.flush();

        assertFalse(OrderUtils.isValidFeeFile(feeFile));
    }


    @Test
    void testRangesFees() throws IOException {
        Path feeFile = Files.createTempFile("DummyPostalService", "test.txt");
        FileWriter writer = new FileWriter(feeFile.toFile());
        writer.append("10.000 5.00\n");
        writer.append("5.000 2.50\n");
        writer.append("2.000 1.55\n");
        writer.append("1.000 1.00\n");
        writer.flush();

        Map<Range<Double>, BigDecimal> rangeFees = OrderUtils.getRangeFees(feeFile);

        assertEquals(5, rangeFees.size());

        Range<Double> range = getFee(rangeFees, 11);
        assertEquals(new BigDecimal("5.00"), rangeFees.get(range));

        range = getFee(rangeFees, 10.1);
        assertEquals(new BigDecimal("5.00"), rangeFees.get(range));

        range = getFee(rangeFees, 10.0);
        assertEquals(new BigDecimal("5.00"), rangeFees.get(range));

        range = getFee(rangeFees, 9.9);
        assertEquals(new BigDecimal("2.50"), rangeFees.get(range));

        range = getFee(rangeFees, 5.1);
        assertEquals(new BigDecimal("2.50"), rangeFees.get(range));

        range = getFee(rangeFees, 5);
        assertEquals(new BigDecimal("2.50"), rangeFees.get(range));

        range = getFee(rangeFees, 4.999);
        assertEquals(new BigDecimal("1.55"), rangeFees.get(range));

        range = getFee(rangeFees, 2.001);
        assertEquals(new BigDecimal("1.55"), rangeFees.get(range));

        range = getFee(rangeFees, 1.99);
        assertEquals(new BigDecimal("1.00"), rangeFees.get(range));

        range = getFee(rangeFees, 0.9);
        assertEquals(new BigDecimal("1.00"), rangeFees.get(range));

        range = getFee(rangeFees, 0.001);
        assertEquals(new BigDecimal("1.00"), rangeFees.get(range));
    }

    private Range<Double> getFee(Map<Range<Double>, BigDecimal> rangeFees, double weight) {
        return rangeFees.keySet().stream()
                .filter(r -> r.contains(weight))
                .findFirst()
                .get();
    }
}