package cz.bsc.external;

import cz.bsc.external.order.PostalOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.jupiter.api.Assertions.*;

public class DummyPostalServiceTest {
    private Queue<PostalOrder> queue = new ConcurrentLinkedQueue<>();
    private DummyPostalService service = new DummyPostalService(queue, Collections.emptyMap());

    @BeforeEach
    void setUp() {
        queue.clear();
    }

    @Test
    void testPostalCodeStartingWithZero() {
        boolean isSuccessful = service.processOrder("100.000 00011");
        assertTrue(isSuccessful);
        assertEquals(1, queue.size());

        PostalOrder order = queue.poll();
        assertNotNull(order);
        assertEquals(order.getWeight(), 100.0);
        assertEquals(order.getPostalCode(), "00011");
    }

    @Test
    void testProcessingInBatch() throws IOException {
        Path ordersFile = Files.createTempFile("DummyPostalService", "test.txt");
        FileWriter writer = new FileWriter(ordersFile.toFile());
        writer.append("2 90005\n");
        writer.append("12.56 08801\n");
        writer.append("5.5 08079\n");
        writer.flush();

        assertTrue((service.processInBatch(ordersFile)), "valid order file");
        assertEquals(3, queue.size());
        PostalOrder order = queue.poll();
        assertNotNull(order);
        assertEquals("90005", order.getPostalCode());
        assertEquals(2, order.getWeight());

        order = queue.poll();
        assertNotNull(order);
        assertEquals("08801", order.getPostalCode());
        assertEquals(12.56, order.getWeight());

        order = queue.poll();
        assertNotNull(order);
        assertEquals("08079", order.getPostalCode());
        assertEquals(5.5, order.getWeight());
    }
}